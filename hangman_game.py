import random
from hangman_extra import hangman_pictures, words


def cls():
    print('\n' * 25)


def get_random(word_list):
    return random.choice(word_list).upper()


def display_hangman(hangman_map, missed_letters, correct_letters, secret_word):
    cls()
    print('    H A N G M A N')
    print(hangman_map[len(missed_letters)], '\n')
    empty_word = '_' * len(secret_word)

    for i in range(len(secret_word)):  # replace blanks with correctly guessed letters
        if secret_word[i] in correct_letters:
            empty_word = empty_word[:i] + secret_word[i] + empty_word[i+1:]
    print('  Word:', end=' ')
    for letter in empty_word:  # show the secret word with spaces in between each letter
        print(letter, end=' ')

    print()
    print('Misses:', end=' ')
    for letter in missed_letters:
        print(letter, end=' ')
    print()


def get_guess(already_guessed):
    while True:
        guess = input(' Guess? ').upper()
        if len(guess) != 1:
            print('Please enter a single letter.')
        elif guess in already_guessed:
            print('You have already guessed that letter. Choose again.')
        elif not guess.isalpha():
            print('Please enter a LETTER.')
        else:
            return guess


def play_again():
    print()
    print('Do you want to play again? (yes or no)')
    return input().lower().startswith('y')


def main():
    secret_word = get_random(words)
    missed_letters = ''
    first_letter = secret_word[0]
    last_letter = secret_word[-1]
    correct_letters = first_letter + last_letter
    game_finished = False

    while not game_finished:
        display_hangman(hangman_pictures, missed_letters, correct_letters, secret_word)
        guess = get_guess(missed_letters + correct_letters)

        if guess in secret_word:
            correct_letters += guess
            win = True
            for i in range(len(secret_word)):
                if secret_word[i] not in correct_letters:
                    win = False
                    break
            if win:
                display_hangman(hangman_pictures, missed_letters, correct_letters, secret_word)
                print()
                print(f'You have won! Congrats!\nThe secret word is indeed "{secret_word}"!')
                game_finished = True
        else:
            missed_letters += guess

            if len(missed_letters) == len(hangman_pictures) - 1:
                display_hangman(hangman_pictures, missed_letters, correct_letters, secret_word)
                print()
                print(f'You lost! You are hanged!\nThe word was "{secret_word}"')
                game_finished = True

        if game_finished:
            if play_again():
                main()
            else:
                print()
                print('Thank you for playing with us! You are awesome!')
                break


main()
